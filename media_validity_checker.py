import os
import win32file
from subprocess import Popen, PIPE
import argparse

import logging
from datetime import datetime

if __name__ == '__main__':
    try:
        start_time = datetime.now()
        parser = argparse.ArgumentParser(description='This tool will target a directory and check the specific files based on extension against the ImageMagick utility, identify.  This will return a result of OK if the file is found to be uncorrupted based off the file extension type or ERROR if the file is corrupted/incomplete')
        parser.add_argument('-sd', '--source-directory', action='store', dest='source_directory', type=str, help='Source directory contianing PSD files (Format: share/folder1/folder2/folderN')
        parser.add_argument('-fe', '--file-extension', action='store', dest='file_extension', type=str, help='File extension to check')
        parser.add_argument('-i', '--interactive', action='store_true', dest='is_interactive', default=False, help='Enables interacitve input of arguments')
        parser.add_argument('-F', '--fast', action='store_true', dest='is_fast', default=False, help='Enable fast mode(May not catch certain types of file corruption)')
        parser.add_argument('-c', '--collate-errors', action='store_true', dest='is_collate_errors', default=False, help='Collate errors at the end of the log')
        parser.add_argument('--debug', action='store_true', dest='is_debug', default=False, help='Enables debug output of arguments')
        
        arguments = parser.parse_args()
        
        if (arguments.is_interactive):
            arguments.source_directory = input("Path to check?: ")  #path to the root directory to search
            arguments.file_extension = input("File extension to check? ex: .psd : ")
        
        date = datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")
        log_file = "media_checker." + arguments.file_extension + "." + date + ".log"
        logging.basicConfig(
            level=logging.INFO,
            format="%(asctime)s [%(levelname)s] %(message)s",
            handlers=[
                logging.FileHandler(log_file, 'w', 'utf-8'),
                logging.StreamHandler()
            ]
        )    
            
        if (arguments.is_debug):
            logging.info('source_directory = ' + str(arguments.source_directory))
            logging.info('file_extension = ' + str(arguments.file_extension))
            logging.info('log_file = ' + str(log_file))
            logging.info('is_fast = ' + str(arguments.is_fast))
            logging.info('is_interactive = ' + str(arguments.is_interactive))
            logging.info('is_collate_errors = ' + str(arguments.is_collate_errors))
            logging.info('is_debug = ' + str(arguments.is_debug))

        def checkImage(fn):
            try:
                if (win32file.GetCompressedFileSize(fn) > 0):
                    if (arguments.is_fast):
                        proc = Popen(['identify', fn], stdout=PIPE, stderr=PIPE)
                    else: 
                        proc = Popen(['identify', '-verbose', fn], stdout=PIPE, stderr=PIPE)
                out, err = proc.communicate()
                exitcode = proc.returncode
            
            except:
                global stub_count
                stub_count += 1
                exitcode = 0
                out = b'' 
                err = b'' 
            return exitcode, out, err
        
        stub_count = 0
        ok_count = 0
        error_count = 0
        errors = []
        
        for directory, subdirectories, files, in os.walk(arguments.source_directory):
            for file in files:
                if file.lower().endswith(arguments.file_extension.lower()):
                    file_path = os.path.join(directory, file)
                    code, output, error = checkImage(file_path)
                    if str(code) !="0" or str(error, "utf-8") != "":
                        logging.error("ERROR " + file_path)
                        error_count += 1
                        errors.append(file_path)
                    else:
                        logging.info("OK " + file_path)
                        ok_count += 1
        logging.info("")
        logging.info("Scanned " + str(ok_count + error_count - stub_count) + " files in " + str(datetime.now() - start_time))
        logging.info("Number of stub files detected and skipped: " + str(stub_count))
        logging.info("Number of OK files detected: " + str(ok_count - stub_count))
        logging.info("Number of possible corrupted files detected: " + str(error_count))
        logging.info("")
        if (arguments.is_collate_errors):
            for err in errors:
                logging.error(err)    
        logging.info("-------------- FIN --------------")
    except KeyboardInterrupt:
        logging.fatal('Cancelled by user.')
        pass
